#!/bin/bash

function usage() {
    echo "usage: $(basename $0) <ip/host>" 1>&2
    exit 1
}

function test_endpoint() {
    local url=http://${hostaddr}:1404/$1
    curl -i $url
    echo
    echo
}

hostaddr=$1
[ -z $hostaddr ] && usage
which curl &>/dev/null || { echo "must have curl installed" && exit 1; }

echo "running tests against host: $hostaddr"

for endpoint in cpuinfo meminfo uptime; do
    for i in {1..5}; do
        echo "Testing Endpoint $endpoint (#$i)"
        test_endpoint $endpoint
        sleep 1
    done
done