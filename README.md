# Ops 0.1.1

## Tools

OS: Ubuntu Server 14.04.5 LTS 64-bit [vagrant box](https://atlas.hashicorp.com/bento/boxes/ubuntu-14.04)
Language: GO
Framework: Goji
Virtualization: Vagrant & VirtualBox (or VMWare Workstation/Fusion)

## Directory Structure

* `phase1` contains the Phase 1 application blueprint
* `phase2` contains the Phase 2 application with shell provisioner
* `phase3` contains the Phase 3 application with puppet provisioner
