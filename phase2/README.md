# Phase 2 - Shell Provisioning

## Run the API Application

Start the Vagrant environment

```bash
$ vagrant up
```

Select the correct interface to bridge (you need one with internet access)

## Use the API

Use the URL shown after provisioning is complete to use the API

```bash
$ curl http://1.2.3.4:1404/cpuinfo
$ curl http://1.2.3.4:1404/meminfo
$ curl http://1.2.3.4:1404/uptime
```