#!/bin/bash

# configure environment
export GOPATH=/vagrant/vmsensor
export GOROOT=/opt/go
export PATH="$GOROOT/bin:$GOPATH/bin:$PATH"
export TERM=xterm
export VMSENSOR_PORT=1404
export VMSENSOR_TTL=5s
export VMSENSOR_LOG=/var/log/vmsensor.log

# variables for installation
gopkg=go1.8.linux-amd64.tar.gz
gourl=https://storage.googleapis.com/golang/$gopkg
apprepo=https://github.com/mspaulding06/vmsensor
curdir=`pwd`

# prepare go setup
apt-get install -y git make
echo "Downloading Go from $gourl"
wget -q $gourl
(cd /opt && tar -zxvf $curdir/$gopkg)

# prepare code
[ -d $GOPATH ] && rm -rf $GOPATH
mkdir -p $GOPATH/{bin,src/github.com/mspaulding06}
(cd $GOPATH/src/github.com/mspaulding06 && git clone $apprepo)

# build and run
make -C $GOPATH/src/github.com/mspaulding06/vmsensor
ipaddr=$(ip addr | grep eth1$ | awk '{print $2}' | cut -d'/' -f1)
vmsensor -bindip $ipaddr -ttl $VMSENSOR_TTL -port $VMSENSOR_PORT -logfile $VMSENSOR_LOG &
echo "-----------------------------------------"
echo "Access the API from http://${ipaddr}:1404"
echo "-----------------------------------------"