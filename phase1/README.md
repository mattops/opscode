# Phase 1 - Blueprint Application

## Installation Instructions

First, bootstrap the Vagrant environment

```bash
$ vagrant up
$ vagrant ssh
```

Install application prerequisites

```bash
$ wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
$ (cd /opt && sudo tar -zxvf ~/go1.8.linux-amd64.tar.gz)
```

Checkout the application repository (vmsensor)

```bash
$ mkdir -p ~/vmsensor/{bin,src/github.com/mspaulding06}
$ (cd ~/vmsensor/src/github.com/mspaulding06 && git clone https://github.com/mspaulding06/vmsensor)
```

Setup Environment

```bash
$ export GOROOT=/opt/go
$ export GOPATH=~/vmsensor
$ export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
```

Build Web Application

```bash
$ cd $GOPATH/src/github.com/mspaulding06/vmsensor
$ make
```

## Usage Instructions

Run Web Application

```bash
$ vmsensor &
```

Help with Flags

```bash
$ vmsensor -h
```

Run API Test from Host

```bash
$ exit
$ ../runtests.sh
```
