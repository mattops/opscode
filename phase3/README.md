# Phase 3 - Puppet Apply Provisioning

## Run the API Application

Start the Vagrant environment

```bash
$ vagrant up
```

Select the correct interface to bridge (you need one with internet access).

The url to use for the API will be shown in a puppet notification message once the environment has been bootstrapped.

## Use the API

Use the URL shown after provisioning is complete to use the API

```bash
$ curl http://1.2.3.4:1404/cpuinfo
$ curl http://1.2.3.4:1404/meminfo
$ curl http://1.2.3.4:1404/uptime
```

## Notes

In this phase vmsensor is deployed differently than in phases 1 and 2.  Here I have uploaded a binary release of the application to simplify deployment.