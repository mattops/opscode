# vmsensor::params
class vmsensor::params {
    $package_url  = 'https://github.com/mspaulding06/vmsensor/releases/download/v0.0.1/vmsensor-linux-amd64.tar.gz'
    $package_name = 'vmsensor-linux-amd64.tar.gz'
    $upstart_conf = 'vmsensor.conf'
}