# vmsensor class
class vmsensor (
    $package_url  = $::vmsensor::params::package_url,
    $package_name = $::vmsensor::params::package_name,
    $upstart_conf = $::vmsensor::params::upstart_conf,
) inherits vmsensor::params {
    include archive

    file { '/opt/vmsensor/':
        ensure => directory,
        mode => 0755,
    }

    archive { "/tmp/${package_name}":
        ensure => present,
        extract => true,
        extract_path => '/opt/vmsensor/',
        source => $package_url,
        cleanup => true,
        require => File['/opt/vmsensor/'],
    }

    file { '/opt/vmsensor/vmsensor':
        ensure => present,
        mode => 0755,
        require => Archive["/tmp/${package_name}"],
    }

    file { "/etc/init/${upstart_conf}":
        ensure => present,
        content => file("vmsensor/${upstart_conf}"),
        mode => 0644,
    }

    service { 'vmsensor':
        ensure => running,
        enable => true,
        provider => 'upstart',
        require => [
            File["/etc/init/${upstart_conf}"],
            File['/opt/vmsensor/vmsensor'],
        ],
    } ->

    notify { 'notification':
        message => "Access the API from http://${::ipaddress_eth1}:1404/",
    }
}
